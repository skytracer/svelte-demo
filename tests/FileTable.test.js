import { cleanup, render } from "@testing-library/svelte";
import userEvent from "@testing-library/user-event";
import FileTable from "../src/components/FileTable.svelte";
afterEach(() => {
  cleanup();
});
const files = [
  {
    name: "smss.exe",
    device: "Stark",
    path: "\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe",
    status: "scheduled",
  },

  {
    name: "netsh.exe",
    device: "Targaryen",
    path: "\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe",
    status: "available",
  },

  {
    name: "uxtheme.dll",
    device: "Lanniester",
    path: "\\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll",
    status: "available",
  },

  {
    name: "cryptbase.dll",
    device: "Martell",
    path: "\\Device\\HarddiskVolume1\\Windows\\System32\\cryptbase.dll",
    status: "scheduled",
  },

  {
    name: "7za.exe",
    device: "Baratheon",
    path: "\\Device\\HarddiskVolume1\\temp\\7za.exe",
    status: "scheduled",
  },
];

it("should have the same number of rows as data", async () => {
  const r = render(FileTable, { files });
  const rows = document.querySelectorAll("tr");
  expect(rows.length).toEqual(files.length + 1);
});

it("should have disabled checkboxes", () => {
  const r = render(FileTable, { files });
  const cbs = document.querySelectorAll("table input");
  files.forEach((f, k) => {
    expect(cbs[k].disabled).toEqual(f.status !== "available");
  });
});

it("should reflect select-all checkbox", async () => {
  const r = render(FileTable, { files });
  expect(
    document.querySelector(".select-all_cb label").getAttribute("aria-label")
  ).toEqual(`select all`);
  await userEvent.click(document.querySelector(".select-all_cb input"));
  expect(document.querySelectorAll("tr[aria-selected=true]").length).toEqual(
    files.filter((f) => f.status === "available").length
  );
  expect(
    document.querySelector(".select-all_cb label").getAttribute("aria-label")
  ).toEqual(`deselect all`);
  await userEvent.click(document.querySelector(".select-all_cb input"));
  expect(document.querySelectorAll("tr[aria-selected=true]").length).toEqual(0);
  await userEvent.click(document.querySelector("table input:not(:disabled)"));
  expect(document.querySelector(".select-all_cb input")).toHaveClass(
    "indeterminate"
  );
  expect(
    document.querySelector(".select-all_cb label").getAttribute("aria-label")
  ).toEqual(`select all`);
  await userEvent.click(document.querySelector(".select-all_cb input"));
  expect(document.querySelectorAll("tr[aria-selected=true]").length).toEqual(
    files.filter((f) => f.status === "available").length
  );
  expect(
    document.querySelector(".select-all_cb label").getAttribute("aria-label")
  ).toEqual(`deselect all`);
});

it("should reflect status when checked", async () => {
  const r = render(FileTable, { files });
  expect(document.querySelector("[role=status]")).toHaveTextContent(
    "None Selected"
  );
  await userEvent.click(document.querySelector(".select-all_cb input"));
  expect(document.querySelector("[role=status]")).toHaveTextContent(
    "Selected 2"
  );
  await userEvent.click(document.querySelector("table input:not(:disabled)"));
  expect(document.querySelector("[role=status]")).toHaveTextContent(
    "Selected 1"
  );
});

it("should reflect download ability", async () => {
  const r = render(FileTable, { files });
  let selectedFiles = [];
  const onDownload = jest.fn((ev) => {
    selectedFiles = ev.detail;
  });
  r.component.$on("download", onDownload);
  const btn = document.querySelector("button");
  await userEvent.click(btn);
  expect(onDownload).not.toHaveBeenCalled();
  await userEvent.click(document.querySelector(".select-all_cb input"));
  await userEvent.click(btn);
  expect(onDownload).toHaveBeenCalled();
  expect(selectedFiles).toEqual(files.filter((f) => f.status === "available"));
});
