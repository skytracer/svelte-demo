import { render, cleanup } from "@testing-library/svelte";
import userEvent from "@testing-library/user-event";
import Checkbox from "../src/components/Checkbox.svelte";
afterEach(() => {
  cleanup();
});
it("should become checked when clicked", async () => {
  const r = render(Checkbox, { checked: false });
  const cb = document.querySelector("input");
  await userEvent.click(cb);
  const lb = document.querySelector("label");
  expect(lb).toHaveTextContent("");
  expect(cb.checked).toEqual(true);
});
it("should become unchecked when clicked", async () => {
  const r = render(Checkbox, { checked: true });
  const cb = document.querySelector("input");
  await userEvent.click(cb);
  expect(cb.checked).toEqual(false);
});
it("should have undeterminate state", async () => {
  const r = render(Checkbox, { checked: null });
  const cb = document.querySelector("input");
  expect(cb).toHaveClass("indeterminate");
});
it("should reflect label", async () => {
  const LABEL = "some label";
  const r1 = render(Checkbox, { checked: false, label: LABEL });
  const cb1 = document.querySelector("label");
  expect(cb1).toHaveTextContent(LABEL);
});
it("should become checked when clicked on label", async () => {
  const LABEL = "some label";
  const r = render(Checkbox, { checked: false, label: LABEL });
  const lb = document.querySelector("label");
  const cb = document.querySelector("input");
  await userEvent.click(lb);
  expect(cb.checked).toEqual(true);
});
it("should not checked when disabled", async () => {
  const r = render(Checkbox, { checked: false, disabled: true });
  const cb = document.querySelector("input");
  await userEvent.click(cb);
  expect(cb.disabled).toEqual(true);
  expect(cb.checked).toEqual(false);
});
