import { cleanup, render } from "@testing-library/svelte";
import userEvent from "@testing-library/user-event";
import SelecTable from "../src/components/SelecTable.svelte";
afterEach(() => {
  cleanup();
});
const files = [
  {
    name: "smss.exe",
    device: "Stark",
    path: "\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe",
    status: "scheduled",
  },

  {
    name: "netsh.exe",
    device: "Targaryen",
    path: "\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe",
    status: "available",
  },

  {
    name: "uxtheme.dll",
    device: "Lanniester",
    path: "\\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll",
    status: "available",
  },

  {
    name: "cryptbase.dll",
    device: "Martell",
    path: "\\Device\\HarddiskVolume1\\Windows\\System32\\cryptbase.dll",
    status: "scheduled",
  },

  {
    name: "7za.exe",
    device: "Baratheon",
    path: "\\Device\\HarddiskVolume1\\temp\\7za.exe",
    status: "scheduled",
  },
];
const columns = [
  {
    field: "name",
    label: "Name",
    dataClass: (d, c) => `select_cell name-${d[c.field]}`,
  },
  { field: "device", label: "Device" },
  { field: "path", label: "Path" },
  {
    field: "status",
    label: "Status",
  },
];

it("should have the same number of rows as data", async () => {
  const r = render(SelecTable, { data: files, columns });
  const rows = document.querySelectorAll("tr");
  expect(rows.length).toEqual(files.length + 1);
  expect(document.querySelector("table")).toHaveAttribute(
    "aria-rowcount",
    String(files.length)
  );
});
it("should have disabled checkboxes", () => {
  const r = render(SelecTable, {
    data: files,
    columns,
    canSelect: (d) => d.device === "Stark",
  });
  const cbs = document.querySelectorAll("input");

  files.forEach((f, k) => {
    expect(cbs[k].disabled).toEqual(k !== 0);
  });
});
it("should have ariaLabels", () => {
  const r = render(SelecTable, {
    data: files,
    columns,
    ariaLabelSelect: (d) => `select ${d.name}`,
  });
  const cbs = document.querySelectorAll("label");
  files.forEach((f, k) => {
    expect(cbs[k].getAttribute("aria-label")).toEqual(`select ${f.name}`);
  });
});
it("should set class from dataClass", () => {
  const r = render(SelecTable, {
    data: files,
    columns,
  });
  const tds = document.querySelectorAll(".select_cell");
  files.forEach((f, k) => {
    expect(tds[k]).toHaveClass(`name-${f.name}`);
  });
  const columns2 = columns.map((c) => {
    return {
      field: c.field,
      label: c.label,
    };
  });
  r.rerender({
    data: files,
    columns: columns2,
  });
  const tds2 = document.querySelectorAll(".select_cell");
  expect(tds2.length).toEqual(0);
});
it("should select all possible", async () => {
  const canSelect = (d) => d.device === "Stark";
  const r = render(SelecTable, {
    data: files,
    columns,
    canSelect,
  });
  const cbs = document.querySelectorAll("input");
  for (let k = 0; k < files.length; k++) {
    await userEvent.click(cbs[k]);
  }
  const tds = document.querySelectorAll("tr[aria-selected=true]");
  expect(tds.length).toEqual(files.filter(canSelect).length);
});
